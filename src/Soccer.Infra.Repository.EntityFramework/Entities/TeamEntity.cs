﻿namespace Soccer.Infra.Repository.EntityFramework.Entities
{
    public class TeamEntity
    {
        public int Id { get; set; }
        public string Code { get; set; }
    }
}