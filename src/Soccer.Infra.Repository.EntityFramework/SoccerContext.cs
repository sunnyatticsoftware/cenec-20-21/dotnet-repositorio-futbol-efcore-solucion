﻿using Microsoft.EntityFrameworkCore;
using Soccer.Infra.Repository.EntityFramework.Entities;

namespace Soccer.Infra.Repository.EntityFramework
{
    public class SoccerContext
        : DbContext
    {
        public SoccerContext(DbContextOptions<SoccerContext> options) 
            : base(options)
        {
        }
        
        // Añade DbSet por cada tabla que consideres oportuna
        public DbSet<TeamEntity> Teams { get; set; }
        public DbSet<GoalEntity> Goals { get; set; }
        public DbSet<GameEntity> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<GameEntity>()
                .HasOne<TeamEntity>(x => x.LocalTeam)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            
            modelBuilder
                .Entity<GameEntity>()
                .HasOne<TeamEntity>(x => x.ForeignTeam)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}